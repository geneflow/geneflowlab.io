
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Build a Container for an App &#8212; GeneFlow 2.3.0 documentation</title>
    <link rel="stylesheet" href="../../_static/pygments.css" type="text/css" />
    <link rel="stylesheet" href="../../_static/classic.css" type="text/css" />
    
    <script id="documentation_options" data-url_root="../../" src="../../_static/documentation_options.js"></script>
    <script src="../../_static/jquery.js"></script>
    <script src="../../_static/underscore.js"></script>
    <script src="../../_static/doctools.js"></script>
    
    <link rel="index" title="Index" href="../../genindex.html" />
    <link rel="search" title="Search" href="../../search.html" />
    <link rel="next" title="Tapis Workflow Installation and Execution" href="tapis-execution.html" />
    <link rel="prev" title="Executing Containers in Apps" href="app-exec-container.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="tapis-execution.html" title="Tapis Workflow Installation and Execution"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="app-exec-container.html" title="Executing Containers in Apps"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="../../index.html">GeneFlow 2.3.0 documentation</a> &#187;</li>
          <li class="nav-item nav-item-1"><a href="../tutorials.html" accesskey="U">GeneFlow Tutorials</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Build a Container for an App</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="build-a-container-for-an-app">
<h1>Build a Container for an App<a class="headerlink" href="#build-a-container-for-an-app" title="Permalink to this headline">¶</a></h1>
<p>In this tutorial, we will build a custom container using Singularity. This tutorial will demonstrate basic concepts: (1) how to choose a base image to build from, (2) how to install a software package from github, (3) how to install packages, and (4) how to copy local files into the container. This tutorial is only meant to be an introduction. For more information on Singularity, check out the documentation at: <a class="reference external" href="https://sylabs.io/docs/">https://sylabs.io/docs/</a> and use Google as usual. Once the container has been created, it can be used in a GeneFlow app.</p>
<div class="section" id="singularity-requirement">
<h2>Singularity Requirement<a class="headerlink" href="#singularity-requirement" title="Permalink to this headline">¶</a></h2>
<p>This tutorial is based on Singularity 2.6. It assumes that you already have Singularity installed and that you have sufficient privileges to build an image (which may require sudo access). Check the Singularity documentation on how to install Singularity or check with your system administrator.</p>
</div>
<div class="section" id="a-container-for-rna-seq">
<h2>A Container for RNA-Seq<a class="headerlink" href="#a-container-for-rna-seq" title="Permalink to this headline">¶</a></h2>
<p>In this tutorial, we will build a container with the following software:</p>
<ol class="arabic simple">
<li><p>STAR aligner (<a class="reference external" href="https://github.com/alexdobin/STAR">https://github.com/alexdobin/STAR</a>)</p></li>
<li><p>DESeq2 on Bioconductor/R (<a class="reference external" href="https://bioconductor.org/packages/release/bioc/html/DESeq2.html">https://bioconductor.org/packages/release/bioc/html/DESeq2.html</a>)</p></li>
<li><p>All dependencies required by the above 2 programs</p></li>
</ol>
</div>
<div class="section" id="singularity-recipe">
<h2>Singularity Recipe<a class="headerlink" href="#singularity-recipe" title="Permalink to this headline">¶</a></h2>
<p>A Singularity recipe is a set of instructions for building the Singularity image. The final Singularity recipe for building the image for the tutorial is shown below. I will break down what each section of the file does below. Meanwhile, copy it into a text file named <cite>Singularity</cite> using your favorite editor. The example here will use vi.</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>vi Singularity
</pre></div>
</div>
<p>Copy the following text into the file and save it.</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>Bootstrap: docker
From: r-base:3.6.0

%files

  test.txt /opt/test.txt

%environment

  TMPDIR=/opt
  export TMPDIR

%labels

   AUTHOR user

%post

  #install STAR
  cd /opt/
  wget https://github.com/alexdobin/STAR/archive/2.7.2b.tar.gz
  tar -xzf 2.7.2b.tar.gz
  rm 2.7.2b.tar.gz
  ln -s /opt/STAR-2.7.2b/bin/Linux_x86_64/STAR /bin/STAR

  #Install dependencies packages
  apt-get update
  apt-get install -y libcurl4-openssl-dev
  apt-get install -y libxml2-dev

  #Install R packages
  export TMPDIR=/opt
  R --slave -e &#39;install.packages(c(&quot;BiocManager&quot;,&quot;docopt&quot;,&quot;stringi&quot;, &quot;stringr&quot;))&#39;
  R --slave -e &#39;BiocManager::install(c(&quot;DESeq2&quot;))&#39;

%runscript

  echo &quot;the fruits of your success will be in direct ratio to the honesty and sincerity of your own effort in keeping your own records, doing your own thinking, and reaching your own conclusions. - Jesse Livermore&quot;
</pre></div>
</div>
</div>
<div class="section" id="base-container">
<h2>Base Container<a class="headerlink" href="#base-container" title="Permalink to this headline">¶</a></h2>
<p>While it is possible to build a container from just a base operating system, it is often easier to start from images that already contain some of the software you want. In this case, we will start from the container image with R already installed. The image we will use is here: <a class="reference external" href="https://hub.docker.com/_/r-base">https://hub.docker.com/_/r-base</a>. The following code section in the recipe file tells Singularity to use this base image. The <cite>Bootstrap:</cite> option is set as “docker” to signify that we are building from a pre-existing docker image from DockerHub. The <cite>From:</cite> options is set as “r-base:3.6.0” to signify that we want to use the r-base container tagged at 3.6.0.</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>Bootstrap: docker
From: r-base:3.6.0
</pre></div>
</div>
</div>
<div class="section" id="copy-files">
<h2>Copy Files<a class="headerlink" href="#copy-files" title="Permalink to this headline">¶</a></h2>
<p>Although the image in this tutorial doesn’t need any local files, you will often want to include some local files (a script for example) in your Singularity image. Therefore, we will copy a dummy file to demonstrate how to copy a file from the local directory into the docker container. Start by making a dummy file with the command:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>echo &quot;This is a test file&quot; &gt; test.txt
</pre></div>
</div>
<p>The section of the recipe file instructing Singularity to copy the file into the image is shown below. Under the <cite>%file</cite> section, specify the source and the destination separated by space. I generally copy files into the <cite>/opt/</cite> directory because most pre-built images have this directory.</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>%files

  test.txt /opt/test.txt
</pre></div>
</div>
</div>
<div class="section" id="set-environmental-variables">
<h2>Set Environmental Variables<a class="headerlink" href="#set-environmental-variables" title="Permalink to this headline">¶</a></h2>
<p>The <cite>%environment</cite> section sets the environmental variables for your image at runtime (but not build time). I included an example of how to do this, but our image doesn’t really need it.</p>
<div class="highlight-test notranslate"><div class="highlight"><pre><span></span>%environment

  TMPDIR=/opt
  export TMPDIR
</pre></div>
</div>
</div>
<div class="section" id="metadata">
<h2>Metadata<a class="headerlink" href="#metadata" title="Permalink to this headline">¶</a></h2>
<p>The <cite>%labels</cite> section contains all of the metadata for the image. In this case, I put in my information as the author.</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>%labels

   AUTHOR user
</pre></div>
</div>
</div>
<div class="section" id="install-your-software">
<h2>Install your software<a class="headerlink" href="#install-your-software" title="Permalink to this headline">¶</a></h2>
<p>The <cite>%post</cite> section contains commands that are executed on top of the base image. This is where most of the setup is done. Our base image is an Ubuntu OS with R installed. Imagine we are running such a computer: what commands do we need to execute to install everything we want?</p>
<p>In the first section of the code:</p>
<ol class="arabic simple">
<li><p>We go to the /opt directory</p></li>
<li><p>Download the STAR tarball</p></li>
<li><p>Unzip the tarball to get the binary</p></li>
<li><p>Remove the tarball</p></li>
<li><p>Softlink the executable STAR binary into the /bin directory so we can execute it from the command line.</p></li>
</ol>
<p>In the second section of the code:</p>
<ol class="arabic simple">
<li><p>We update the list of libraries for the Ubuntu OS</p></li>
<li><p>Install the libcurl4-openssl-dev library</p></li>
<li><p>Install the libxml2-dev library (both needed by R packages)</p></li>
</ol>
<p>In the final section of the code:</p>
<ol class="arabic simple">
<li><p>We export and set <cite>TMPDIR</cite> as “opt” because R will download and compile packages in the directory specified by the TMPDIR variable, and /tmp is often set as noexec</p></li>
<li><p>We install the R packages (including bioconductor)</p></li>
<li><p>We install the Bioconductor package DESeq2</p></li>
</ol>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>%post

  #install STAR
  cd /opt/
  wget https://github.com/alexdobin/STAR/archive/2.7.2b.tar.gz
  tar -xzf 2.7.2b.tar.gz
  rm 2.7.2b.tar.gz
  ln -s /opt/STAR-2.7.2b/bin/Linux_x86_64/STAR /bin/STAR

  #Install dependencies packages
  apt-get update
  apt-get install -y libcurl4-openssl-dev
  apt-get install -y libxml2-dev

  #Install R packages
  export TMPDIR=/opt
  R --slave -e &#39;install.packages(c(&quot;BiocManager&quot;,&quot;docopt&quot;,&quot;stringi&quot;, &quot;stringr&quot;))&#39;
  R --slave -e &#39;BiocManager::install(c(&quot;DESeq2&quot;))&#39;
</pre></div>
</div>
</div>
<div class="section" id="container-as-an-executable">
<h2>Container as an Executable<a class="headerlink" href="#container-as-an-executable" title="Permalink to this headline">¶</a></h2>
<p>The <cite>%runscript</cite> section defines what commands are executed if the image is run as an executable (see below). We echo a quote to demonstrate this function.</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>%runscript

  echo &quot;the fruits of your success will be in direct ratio to the honesty and sincerity of your own effort in keeping your own records, doing your own thinking, and reaching your own conclusions. - Jesse Livermore&quot;
</pre></div>
</div>
</div>
<div class="section" id="build-your-image">
<h2>Build your image<a class="headerlink" href="#build-your-image" title="Permalink to this headline">¶</a></h2>
<p>Assuming you named your recipe file “Singularity”, execute the following command to build your image (“STAR-DESeq2.img”). This will take some time and you will need to have sudo access.</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>sudo singularity build STAR-DESeq2.img Singularity
</pre></div>
</div>
</div>
<div class="section" id="working-with-your-image">
<h2>Working with your image<a class="headerlink" href="#working-with-your-image" title="Permalink to this headline">¶</a></h2>
<p>There are 3 main ways to interact with a Singularity image. Choose the method that suits your goals. We will briefly explore all three.</p>
<div class="section" id="shell">
<h3>Shell<a class="headerlink" href="#shell" title="Permalink to this headline">¶</a></h3>
<p>You can interactively shell into your image using the following command.</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>singularity shell STAR-DESeq2.img
</pre></div>
</div>
<p>Feel free to explore your virtual image. Try calling the manual of STAR with the following command:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>STAR -h
</pre></div>
</div>
<p>Echo the environment variable you set with the following command:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>echo $TMPDIR
</pre></div>
</div>
<p>Check whether the test.txt got copied by going into the /opt directory:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>cd /opt
ls
</pre></div>
</div>
<p>Run R and check if DESeq2 is available with the following commands. Exit R with the <cite>quit()</cite> command.</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>R
library(&quot;DESeq2&quot;)
</pre></div>
</div>
<p>Exit the shell with <cite>exit</cite> when you are done exploring.</p>
</div>
<div class="section" id="run">
<h3>Run<a class="headerlink" href="#run" title="Permalink to this headline">¶</a></h3>
<p>The <cite>singularity run</cite> command executes the commands in the <cite>%runscript%</cite> section. Running the following command should echo the quote we put in our <cite>%runscript%</cite> section.</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>singularity run STAR-DESeq2.img
</pre></div>
</div>
</div>
<div class="section" id="exec">
<h3>Exec<a class="headerlink" href="#exec" title="Permalink to this headline">¶</a></h3>
<p>The <cite>singularity exec [IMAGE] [CMD]</cite> command executes the command from the environment defined in the image. For example, the command below executes the STAR command from the STAR-DESeq2.img with the -h flag.</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>singularity exec STAR-DESeq2.img STAR -h
</pre></div>
</div>
</div>
</div>
<div class="section" id="summary">
<h2>Summary<a class="headerlink" href="#summary" title="Permalink to this headline">¶</a></h2>
<p>After this tutorial, you should know the basics of how to build and run a Singularity image. Note that building a complex image can be a frustrating experience because we take for granted the dependencies our programs need and are pre-installed on most computers. A container image will often require finding out every dependency (and their dependencies) and installing all of them. Try finding pre-existing containers whenever you can. A good resource for bioinformatic containers is <a class="reference external" href="https://quay.io/organization/biocontainers">https://quay.io/organization/biocontainers</a>.</p>
</div>
</div>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <h3><a href="../../index.html">Table of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Build a Container for an App</a><ul>
<li><a class="reference internal" href="#singularity-requirement">Singularity Requirement</a></li>
<li><a class="reference internal" href="#a-container-for-rna-seq">A Container for RNA-Seq</a></li>
<li><a class="reference internal" href="#singularity-recipe">Singularity Recipe</a></li>
<li><a class="reference internal" href="#base-container">Base Container</a></li>
<li><a class="reference internal" href="#copy-files">Copy Files</a></li>
<li><a class="reference internal" href="#set-environmental-variables">Set Environmental Variables</a></li>
<li><a class="reference internal" href="#metadata">Metadata</a></li>
<li><a class="reference internal" href="#install-your-software">Install your software</a></li>
<li><a class="reference internal" href="#container-as-an-executable">Container as an Executable</a></li>
<li><a class="reference internal" href="#build-your-image">Build your image</a></li>
<li><a class="reference internal" href="#working-with-your-image">Working with your image</a><ul>
<li><a class="reference internal" href="#shell">Shell</a></li>
<li><a class="reference internal" href="#run">Run</a></li>
<li><a class="reference internal" href="#exec">Exec</a></li>
</ul>
</li>
<li><a class="reference internal" href="#summary">Summary</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="app-exec-container.html"
                        title="previous chapter">Executing Containers in Apps</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="tapis-execution.html"
                        title="next chapter">Tapis Workflow Installation and Execution</a></p>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../../_sources/chapters/tutorials/build-app-container.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="tapis-execution.html" title="Tapis Workflow Installation and Execution"
             >next</a> |</li>
        <li class="right" >
          <a href="app-exec-container.html" title="Executing Containers in Apps"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="../../index.html">GeneFlow 2.3.0 documentation</a> &#187;</li>
          <li class="nav-item nav-item-1"><a href="../tutorials.html" >GeneFlow Tutorials</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Build a Container for an App</a></li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
        &#169; Copyright 2020, SCBS.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 3.5.2.
    </div>
  </body>
</html>